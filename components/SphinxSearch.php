<?php
/**
 *
 */
class SphinxSearch extends CApplicationComponent
{
	/**
	 *
	 * @var type
	 */
	public $host = '127.0.0.1';

	/**
	 *
	 * @var type
	 */
	public $port = 9306;

	/**
	 *
	 * @var type
	 */
	public $limit = 20;

	/**
	 *
	 * @var type
	 */
	private $_error = array();

	/**
	 *
	 * @var type
	 */
	private $_dsn;

	/**
	 *
	 * @var type
	 */
	private $_criteria;

	/**
	 *
	 * @var type 
	 */
	private $_criteria_parameters = array(
		'select', 'from', 'where', 'match',
		'group_by', 'order_by', 'offset', 'limit',
	);

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$this->_dsn = 'mysql:host=' . $this->host . ';port=' . $this->port . ';';
		$this->_criteria = new stdClass();
	}

	/**
	 *
	 * @param type $criteria
	 * @return SphinxSearch
	 * @throws SphinxSearchException
	 */
	public function setCriteria($criteria)
	{
		if (!is_object($criteria) && !is_array($criteria)) {
			throw new SphinxSearchException('Criteria must be either an object or an array');
		}

		foreach ($criteria as $key => $value) {
			if (!in_array($key, $this->_criteria_parameters)) {
				throw new SphinxSearchException('Key "' . $key . '" is not valid');
			}

			$this->_criteria->$key = $value;
		}

		return $this;
	}

	/**
	 *
	 * @return type
	 */
	public function query()
	{
		try {
			$connection = new CDbConnection($this->_dsn);
			$result = $connection->createCommand($this->getQuery())->queryAll();
		} catch (Exception $e) {
			$this->_error = array(
				'code' => $e->getCode(),
				'message' => $e->getMessage(),
			);
		}

		return !empty($result) ? $result : false;
	}

	/**
	 *
	 * @return type
	 */
	public function getQuery()
	{
		return	"SELECT " . $this->_buildSelect() . " " .
				"FROM " . $this->_buildFrom() . " " .
				$this->_buildWhere() .
				$this->_buildGroupBy() .
				$this->_buildOrderBy() .
				"LIMIT " . $this->_buildOffset() . ", " . $this->_buildLimit();
	}

	/**
	 *
	 * @return type
	 */
	public function getLastError()
	{
		return $this->_error;
	}

	/**
	 *
	 * @return string
	 */
	private function _buildSelect()
	{
		if (empty($this->_criteria->select)) {
			return '*';
		} elseif (is_string($this->_criteria->select)) {
			return $this->_criteria->select;
		}

		foreach ($this->_criteria->select as $key => &$value) {
			$value .= !is_numeric($key) ? ' AS ' . $key : null;
		}

		return implode(', ', $this->_criteria->select);
	}

	/**
	 *
	 * @return type
	 * @throws SphinxSearchException
	 */
	private function _buildFrom()
	{
		if (empty($this->_criteria->from)) {
			throw new SphinxSearchException('Index to select from was not set');
		}

		return $this->_criteria->from;
	}

	/**
	 *
	 * @return type
	 */
	private function _buildWhere()
	{
		$where = array();

		if (!empty($this->_criteria->where)) {
			if (is_string($this->_criteria->where)) {
				$where[] = $this->_criteria->where;
			} elseif (is_array($this->_criteria->where)) {
				$where = $this->_criteria->where;
			}
		}

		if (!empty($this->_criteria->match) && is_string($this->_criteria->match)) {
			$where[] = 'MATCH(\'' . $this->_criteria->match . '\')';
		}

		return !empty($where) ? 'WHERE ' . implode(' AND ', $where) . ' ' : null;
	}

	/**
	 *
	 * @return type
	 */
	private function _buildGroupBy()
	{
		$group_by = array();

		if (!empty($this->_criteria->group_by)) {
			if (is_string($this->_criteria->group_by)) {
				$group_by[] = $this->_criteria->group_by;
			} elseif (is_array($this->_criteria->group_by)) {
				$group_by = $this->_criteria->group_by;
			}
		}

		return !empty($group_by) ? 'GROUP BY ' . implode(', ', $group_by) . ' ' : null;
	}

	/**
	 *
	 * @return type
	 */
	private function _buildOrderBy()
	{
		$order_by = array();

		if (!empty($this->_criteria->order_by)) {
			if (is_string($this->_criteria->order_by)) {
				$order_by[] = $this->_criteria->order_by;
			} elseif (is_array($this->_criteria->order_by)) {
				$order_by = $this->_criteria->order_by;
			}
		}

		return !empty($order_by) ? 'ORDER BY ' . implode(', ', $order_by) . ' ' : null;
	}

	/**
	 *
	 * @return type
	 */
	private function _buildOffset()
	{
		return !empty($this->_criteria->offset) && is_numeric($this->_criteria->offset) ? $this->_criteria->offset : 0;
	}

	/**
	 *
	 * @return type
	 */
	private function _buildLimit()
	{
		return !empty($this->_criteria->limit) && is_numeric($this->_criteria->limit) ? $this->_criteria->limit : $this->limit;
	}
}

/**
 *
 */
class SphinxSearchException extends CException
{

}